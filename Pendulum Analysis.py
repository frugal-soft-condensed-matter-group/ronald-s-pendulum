import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt


def traj(XM,YM):
    plt.scatter(XM,YM, marker='*', color = 'r', s=6)
    plt.xlabel("x(cm)")
    plt.ylabel("y(cm)")

    plt.xlim(0,45)
    plt.ylim(-20,25)                                #Proportion between axes
    plt.gca().set_aspect('equal', adjustable='box')

    plt.title("Trajectory")
    #plt.savefig('Trajectory0.png')
    #plt.grid(True)
    plt.show()


def new_traj(X,Y,L):
    plt.scatter(X,Y, marker='*', color = 'r', s=6)
    plt.xlabel("x(cm)")
    plt.ylabel("y(cm)")

    plt.xlim(-L,L) #(-20,25)
    plt.axis('scaled')
    
    plt.title("Trajectory")

    angles = np.linspace(0,2*np.pi,100)
    plt.plot(L*np.cos(angles), L*np.sin(angles))

    #plt.savefig('Trajectory1.png')
    plt.show()


def new_traj2(X,Y,L):
    plt.scatter(X,Y, marker='*', color = 'r', s=6)
    plt.xlabel("x(cm)")
    plt.ylabel("y(cm)")

    dx = 5 
    plt.xlim(-L-dx,L+dx)
    plt.ylim(-L-dx,L+dx)                #Proportion between axes
    plt.gca().set_aspect('equal', adjustable='box')
    
    plt.title("Trajectory")

    angles = np.linspace(0,2*np.pi,100)
    plt.plot(L*np.cos(angles), L*np.sin(angles))

    #plt.savefig('Trajectory2.png')
    plt.show()



def xt(X,time_values):
    plt.scatter(time_values, X, marker='*', color = 'r', s=6, label ='x Measured values')
    plt.xlabel("t(s)")
    plt.ylabel("x(cm)")
    plt.title("x as a Function of Time.")

    A_guess = np.array([X.max(),-X.min()]).max()
    theta0_guess = np.arccos(X[0]/A_guess)
    guess = [theta0_guess,A_guess,3]#,np.arccos(-16.5/75)]  #X0 = 16.5   A=75
    params, params_covariance = optimize.curve_fit(lambda t,theta0,A,w: A*np.cos(w*t+theta0) , time_values, X, guess)
    plt.plot(time_values, [params[1]*np.cos(params[2]*t+params[0]) for t in time_values], label ='x Fitted curve')
    print("x(t) = %.2fcos[%.2ft+(%.2f)]"%(params[1],params[2],params[0]))

    plt.legend()
    plt.savefig('xt.png')
    plt.show()


def yt(Y,time_values):
    plt.scatter(time_values, Y, marker='*', color = 'y', s=6, label ='y Measured values')
    plt.xlabel("t(s)")
    plt.ylabel("y (cm)")
    plt.title("y as a Function of Time.")

    B_guess = (Y.max()-Y.min())/2
    h_guess = (Y.max()+Y.min())/2
    phi0_guess = np.arcsin((Y[0]-h_guess)/B_guess)    
    guess = [phi0_guess,B_guess,3,Y.max()] #phi0,B,w,h
    params, params_covariance = optimize.curve_fit(lambda t,phi0,B,w,h: B*np.sin(2*w*t+phi0) +h , time_values, Y, guess)
    plt.plot(time_values, [params[1]*np.sin(2*params[2]*t+params[0]) + params[3] for t in time_values], color = 'c', label ='y Fitted curve')
    print("y(t) = %.2fsin[%.2ft+(%.2f)] + %.2f"%(params[1],params[2],params[0],params[3]))

    plt.legend()
    #plt.savefig('yt.png')
    plt.show()
    


def xyt(X,Y, time_values):
    #X
    plt.scatter(time_values, X, marker='*', color = 'r', s=6, label ='x Measured values')
    
    guess = [0,75,3]#,np.arccos(-16.5/75)]  #X0 = 16.5   A=75
    params, params_covariance = optimize.curve_fit(lambda t,theta0,A,w: A*np.cos(w*t+theta0) , time_values, X, guess)
    plt.plot(time_values, [params[1]*np.cos(params[2]*t+params[0]) for t in time_values], label ='x Fitted curve')

    #Y
    plt.scatter(time_values, Y, marker='*', color = 'y', s=6, label ='y Measured values')
    
    guess = [4,2,3,-85] #phi0,B,w,h
    params, params_covariance = optimize.curve_fit(lambda t,phi0,B,w,h: B*np.sin(2*w*t+phi0) +h , time_values, Y, guess)
    plt.plot(time_values, [params[1]*np.sin(2*params[2]*t+params[0]) + params[3] for t in time_values], color = 'c', label ='y Fitted curve')

    plt.xlabel("t(s)")
    plt.ylabel("x and y (cm)")
    plt.title("x(t) and y(t)")
    plt.legend()
    #plt.savefig('xyt.png')
    plt.show()



def main():
    
    #Reading data from txt file with numpy.
    frame,XM,YM  = np.loadtxt('Results.txt', delimiter = ',', skiprows=1, unpack = True, )

    #Ploting Trajectory.   
    traj(XM,YM)
    
    #Founding the circle's center and its radius.
    guess = [XM.mean(),YM.max()-100,100] #L approx 100 cm
    params, params_covariance = optimize.curve_fit(lambda x,a,b,L: b + np.sqrt(L**2-(x-a)**2) , XM, YM, guess)     #(x-a)² + (y-b)² = L²
    print("[x - (%.2f)]² + [y - (%.2f)]² = (%.2f)²"%(params[0],params[1],params[2]))

    X0 = params[0]
    Y0 = params[1]
    L = params[2]

    #New referencial
    X = XM-X0
    Y = Y0-YM

    #Ploting Trajectory in New Referencial.
    new_traj(X,Y,L)
    new_traj2(X,Y,L)


    ########################################################## 
     #Ploting x and y as a function of time.
    T = 2  #Period of oscilation in seconds
    frame_period = 2*(49-14) #14 and 49 was corresponding frames in wich my y was minimum.
    frame_time = T/frame_period #Time per frame aproximation

    g = (L/100)*(2*np.pi/T)**2
    print("g = %.2fm/s²"%g)

    time_values = [(j-1)*frame_time for j in frame]
    
    #x=x(t)
    xt(X,time_values)
    
    #y=y(t)
    yt(Y,time_values)

    #x(t) and y(t)
    xyt(X,Y,time_values)
      

main()
