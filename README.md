# Pendulum Analysis (Using Fiji)

In this project i'm using Fiji to measure the pendulum's mass central position during his motion.
The mass is a lemon, which is nearly spherical, and a paper clip was used to connect it to a string.
Then we can compare the obtained values with those that would be expected by the theory.



## Requirements

### Hardwares
- Smartphone+support
- Coins, fruits, balls
- Yarn or string
- White screen
- Support for the pendulum
### Softwares
- Fiji with ffmpeg enabled

- Python IDE (Thonny with Matplotlib installed)

  

## Problems Faced

- The video file was not supported, then i activate FFMPG. You can do as follows:

  - Help --> Update --> Manage Update sites --> FFMPG --> close --> Apply changes

  - Now you have to close Fiji.
  - To open the video: File --> Import --> Movie FFMPG
  - This [video](https://www.youtube.com/watch?v=X29zKkvKwEk) can also be helpful.
- My smart phone generates video files in 3gp format. This could be solved downloading the [File Converter](https://file-converter.org/), and then converting to mp4 or avi. 
- The duration of my video was to big for Fiji, 24 seconds and 3,85Mb was the size of the file in AVI. So i cut the video (8 seconds and 1,54Mb) opening the with Photos, a default program in Windowns 10, and editing. Checking the Fiji's limitations with AVI files can help, see it [here](https://imagej.nih.gov/ij/plugins/avi-reader.html).  You can also try some of this free online editors: [kapwing](https://www.kapwing.com/) or [online video cutter](https://online-video-cutter.com/pt/). 
- When i try to save the table of values in csv, to read it with excel, all the values appeared in only one column. I solve it converting the file to txt, changing the file's name (name.csv --> name.txt), then reading it as a txt i press (ctrl+H) to exchange "," by ";" and "." by ",". Was everything in place when i convert back to csv. 
  - Later I did all the data analysis using Python, eliminating the use of Excel.



## Fiji's Useful Features

Here i want to present some interesting and useful Features available in Fiji.

### Taking Measures

- To measure the distance between two pixels with Fiji, first you need to have some distance that you know. In my image, i used the paper clip 's length, i also tried to choose the most defined frame.
- Note that, the known distance must be on the same plane as the one to be measured.
- Click the tool: Straight. The 5th one.
- Draw a line segment at the known distance.
- Now follow the path:  Analyze --> Set Scale
  - Distance in pixels = Distance marked with the Straight's tool.
  - Known distance = Here you put his value, without the dimension. In my case 4.1
  - Pixel aspect ratio = Here i used 1.0
  - Unit os lenght = Now you inform the dimension. In my case cm.
- Mark global and press OK.

### Define what you want to measure.

- Analyze/Set Measurements 
- I just marked:
  - Center of mass
  - Stack Position

### Saving and Converting your Images .

- It may be useful to save the intermediate versions of your file. Or even save your video frames as an image.
- File/Save as/ 

### Focusing on Objects of Interest

- Initially i had a image like in the following picture 

  <img src = "img/Pendulo0.jpg" style="zoom:35%;"  >

- To visualize only my pendulum, i selected a rectangular region and clicked  img/Crop. Or just Crtl+Shift+X.

  ​                                                        <img src = "img/Pendulo1.jpg" style="zoom:40%;" >

- Remove background:

  - Image/Type/ (8bit or 16bit)

  - To put a black background. Image/Lookup Tables/Invert LUT

  - <img src  = "img/Pendulo2.jpg" style="zoom:40%;" >

  - Image/Ajust/Threshold  (or Ctrl+Shift+T)  and mark "Dark Background" option.

    <img src  = "img/Pendulo3.jpg" style="zoom:40%;" >

  - Process/Subtract Background   (With Preview marked ). Change also Rolling Ball Radius, for me 1.0 works. Note that in this way the center of mass will be evaluated only by using the contour points. You can try moving on without doing this step.

    <img src = "img/Pendulo4.png" style="zoom:40%;">

  - Image/Adjust/(Brightness/Contrast)  (or Ctrl+Shift+C). This can be useful for you.

    

  - The video: 

    <img src = "img/Pendulo.mp4">

    

### Obtaining the Center of Mass Position for each Frame

- I selected an area in the video to not include too much of the paper clip.
- Analyze/Tools/ROI Manager
- Add [t]
- More>>     Multi Measure
- You can save as csv but work with txt by renaming the file.



## My Results

After obtain the coordinates with Fiji and save the results in a txt file, i could use Python to plot graphics.

<img src = "img/Trajetoria_1.png">

A  circumference arc appears!

Here we can see the center of mass position, according Fiji, of my pendulum during his trajectory. Note that the y axis is inverted, because in Fiji that axis is oriented downwards.

To found the corresponding circumference we fit this curve with Python. There is a helpful [link](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Linear%20Fitting.py).

To do that i suppose a curve that satisfies $`(x-a)^2+(y-b)^2 = L^2`$.  

Taking the top side, we have $`y = b + \sqrt{L² - (x-a)²}`$.

To compare with something that i known, i calculate the length L of my string.

I got $`a = 21.7cm`$, $`b = -72.3cm`$ and $`L = 87.2cm`$.

Changing my referential, the new origin will be at  $`(a,b)`$ , the center of the circumference. Inverting y axis we found.

<img src = "img/Trajetoria_4.png">

The blue curve is the  found circumference. Which can be seen in full afterwards.

![Trajetória](img/Trajetória.png)

Another interesting result can be obtained by looking for the temporal relationship of the measured quantities.

From Fiji I got the position (x, y) and the frame associated with that position. Knowing that my period is approximately $`T = 2s`$, I checked in Fiji the frame relative to two consecutive minimum points in the trajectory. Then I compared it with the generated table and confirmed that those frames also minimized the measured $`y`$.
This route takes half a period. Of that we have:
$`n t_{frame} = T/2 \implies t_{frame} = T/2n`$

Where n is the number of frames. If $`f_1 = fist frame`$,$`f_2 = second frame`$ then $`n =f_2-f_1`$.

A point in frame $`f`$ is in time $`t = f t_{frame}  `$.

![x=x(t)](img/xt.png)

The expected sinusoidal shape appeared. As we known, the pendulum's position is given by $`(x(t),y(t))=L(\sin[\theta(t)],-\cos[\theta(t)])`$ . Where $`\theta`$ is the angle formed between the line and a vertical line that passes through the node.

For small angles we can take the approximation $`sin(\theta) \approx \theta`$ and so $`x=L\theta `$.

Applying Newton's second law, $`-mgsin(\theta)=mL\alpha`$, where approximating $`\alpha =-\frac{g}{L}\theta`$ is the angular. Rewriting we have the differential equation $`\ddot{x}=-\frac{g}{L}x`$ with solution $`x = Acos(\omega t+\theta_0)`$. The relation between $`\omega`$ and the period is $`\omega=\frac{2\pi}{T}`$.  As the period is approximately 2 seconds we expect that $`\omega \approx \pi`$. After the curve fitting i found $`\omega = 3.22Hz`$ .

We can also find the gravity acceleration with $`\omega^2 = \frac{g}{L} \implies g =L\omega^2 =  L(\frac{2\pi}{T})^2 `$. Got as a result $`g = 8.60 m/s^2`$. 

![y=y(t)](img/yt.png)

For y, I modeled my data with the function $`y = B\sin(2\omega t + \phi_0)+h`$. 

In half oscillation of the pendulum $`y`$ reaches its maximum value twice. From this we can conclude that its angular frequency must be twice the angular frequency of $`x`$.

As for my $`y`$ coordinate system it will never assume a value of 0, since we have small angles, I introduce a term $`h`$ that will make this correction.

For this curve I got $`\omega = 3.14Hz`$.

![x and y](img/xyt.png)

Here we see clearly that the variation in the horizontal axis is much greater than that of the vertical axis.











## The Python Script

```
import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt


def traj(XM,YM):
    plt.scatter(XM,YM, marker='*', color = 'r', s=6)
    plt.xlabel("x(cm)")
    plt.ylabel("y(cm)")

    plt.xlim(0,45)
    plt.ylim(-20,25)                                #Proportion between axes
    plt.gca().set_aspect('equal', adjustable='box')

    plt.title("Trajectory")
    #plt.savefig('Trajectory0.png')
    #plt.grid(True)
    plt.show()


def new_traj(X,Y,L):
    plt.scatter(X,Y, marker='*', color = 'r', s=6)
    plt.xlabel("x(cm)")
    plt.ylabel("y(cm)")

    plt.xlim(-L,L) #(-20,25)
    plt.axis('scaled')
    
    plt.title("Trajectory")

    angles = np.linspace(0,2*np.pi,100)
    plt.plot(L*np.cos(angles), L*np.sin(angles))

    #plt.savefig('Trajectory1.png')
    plt.show()


def new_traj2(X,Y,L):
    plt.scatter(X,Y, marker='*', color = 'r', s=6)
    plt.xlabel("x(cm)")
    plt.ylabel("y(cm)")

    dx = 5 
    plt.xlim(-L-dx,L+dx)
    plt.ylim(-L-dx,L+dx)                #Proportion between axes
    plt.gca().set_aspect('equal', adjustable='box')
    
    plt.title("Trajectory")

    angles = np.linspace(0,2*np.pi,100)
    plt.plot(L*np.cos(angles), L*np.sin(angles))

    #plt.savefig('Trajectory2.png')
    plt.show()



def xt(X,time_values):
    plt.scatter(time_values, X, marker='*', color = 'r', s=6, label ='x Measured values')
    plt.xlabel("t(s)")
    plt.ylabel("x(cm)")
    plt.title("x as a Function of Time.")

    A_guess = np.array([X.max(),-X.min()]).max()
    theta0_guess = np.arccos(X[0]/A_guess)
    guess = [theta0_guess,A_guess,3]#,np.arccos(-16.5/75)]  #X0 = 16.5   A=75
    params, params_covariance = optimize.curve_fit(lambda t,theta0,A,w: A*np.cos(w*t+theta0) , time_values, X, guess)
    plt.plot(time_values, [params[1]*np.cos(params[2]*t+params[0]) for t in time_values], label ='x Fitted curve')
    print("x(t) = %.2fcos[%.2ft+(%.2f)]"%(params[1],params[2],params[0]))

    #plt.savefig('xt.png')
    plt.show()


def yt(Y,time_values):
    plt.scatter(time_values, Y, marker='*', color = 'y', s=6, label ='y Measured values')
    plt.xlabel("t(s)")
    plt.ylabel("y (cm)")
    plt.title("y as a Function of Time.")

    B_guess = (Y.max()-Y.min())/2
    h_guess = (Y.max()+Y.min())/2
    phi0_guess = np.arcsin((Y[0]-h_guess)/B_guess)    
    guess = [phi0_guess,B_guess,3,Y.max()] #phi0,B,w,h
    params, params_covariance = optimize.curve_fit(lambda t,phi0,B,w,h: B*np.sin(2*w*t+phi0) +h , time_values, Y, guess)
    plt.plot(time_values, [params[1]*np.sin(2*params[2]*t+params[0]) + params[3] for t in time_values], color = 'c', label ='y Fitted curve')
    print("y(t) = %.2fsin[%.2ft+(%.2f)] + %.2f"%(params[1],params[2],params[0],params[3]))

    plt.legend()
    #plt.savefig('yt.png')
    plt.show()
    


def xyt(X,Y, time_values):
    #X
    plt.scatter(time_values, X, marker='*', color = 'r', s=6, label ='x Measured values')
    
    guess = [0,75,3]#,np.arccos(-16.5/75)]  #X0 = 16.5   A=75
    params, params_covariance = optimize.curve_fit(lambda t,theta0,A,w: A*np.cos(w*t+theta0) , time_values, X, guess)
    plt.plot(time_values, [params[1]*np.cos(params[2]*t+params[0]) for t in time_values], label ='x Fitted curve')

    #Y
    plt.scatter(time_values, Y, marker='*', color = 'y', s=6, label ='y Measured values')
    
    guess = [4,2,3,-85] #phi0,B,w,h
    params, params_covariance = optimize.curve_fit(lambda t,phi0,B,w,h: B*np.sin(2*w*t+phi0) +h , time_values, Y, guess)
    plt.plot(time_values, [params[1]*np.sin(2*params[2]*t+params[0]) + params[3] for t in time_values], color = 'c', label ='y Fitted curve')

    plt.xlabel("t(s)")
    plt.ylabel("x and y (cm)")
    plt.title("x(t) and y(t)")
    plt.legend()
    #plt.savefig('xyt.png')
    plt.show()



def main():
    
    #Reading data from txt file with numpy.
    frame,XM,YM  = np.loadtxt('Results.txt', delimiter = ';', skiprows=1, unpack = True, )

    #Ploting Trajectory.   
    traj(XM,YM)
    
    #Founding the circle's center and its radius.
    guess = [XM.mean(),YM.max()-100,100] #L approx 100 cm
    params, params_covariance = optimize.curve_fit(lambda x,a,b,L: b + np.sqrt(L**2-(x-a)**2) , XM, YM, guess)     #(x-a)² + (y-b)² = L²
    print("[x - (%.2f)]² + [y - (%.2f)]² = (%.2f)²"%(params[0],params[1],params[2]))

    X0 = params[0]
    Y0 = params[1]
    L = params[2]

    #New referencial
    X = XM-X0
    Y = Y0-YM

    #Ploting Trajectory in New Referencial.
    new_traj(X,Y,L)
    new_traj2(X,Y,L)


    ########################################################## 
     #Ploting x and y as a function of time.
    T = 2  #Period of oscilation in seconds
    frame_period = 2*(49-14) #14 and 49 was corresponding frames in wich my y was minimum.
    frame_time = T/frame_period #Time per frame aproximation

    g = (L/100)*(2*np.pi/T)**2
    print("g = %.2fm/s²"%g)

    time_values = [(j-1)*frame_time for j in frame]
    
    #x=x(t)
    xt(X,time_values)
    
    #y=y(t)
    yt(Y,time_values)

    #x(t) and y(t)
    xyt(X,Y,time_values)
      

main()

```



This alternative algorithm, written by Ajax, can be useful for you.

```
### Data analysis with Python

​```python
import glob
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

filename = glob.glob("./Data/*.csv")
data = pd.read_csv(filename[0]).to_numpy()
X = data[:,1]
Y = data[:,2]

font_prop = font_manager.FontProperties(size=14)
fontsize = 12;

fig, ax = plt.subplots()
ax.scatter(X,Y, label = "Trajectory of the mass of a pendulum")
plt.xticks(fontsize= fontsize)
plt.yticks(fontsize= fontsize)
ax.set_xlabel('x axis',fontsize = fontsize)
ax.set_ylabel('z axis',fontsize = fontsize)
ax.set_aspect('auto')
ax.legend(loc='lower left', prop={'size': fontsize})

plt.show()
​```
```